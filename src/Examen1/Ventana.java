package Examen1;
import java.awt.Button;
import java.util.Random;
import javax.swing.JOptionPane;

/**
 *
 * @author daira
 */
public class Ventana extends javax.swing.JFrame {
    private char operadores[] = {'*', '+', '-', '/'};
    private int aciertos = 0;
    private int oportunidad = 3;
    public Ventana() {
        initComponents();
    
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        numero1 = new javax.swing.JTextField();
        signo = new javax.swing.JTextField();
        numero2 = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        respuesta = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        correcta = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        oportunidades = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        darRespuesta = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        signo.setText("*");

        jLabel1.setText("Respuesta Correcta");

        jLabel2.setText("Oportunidades Restantes");

        oportunidades.setText("3");

        jButton1.setText("Iniciar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        darRespuesta.setText("Dar Respuesta");
        darRespuesta.setEnabled(false);
        darRespuesta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                darRespuestaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(numero1)
                            .addComponent(signo, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)
                            .addComponent(numero2)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(respuesta, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(darRespuesta)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(correcta, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(oportunidades, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jButton1)))
                .addGap(51, 51, 51))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(numero1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(correcta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(signo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(numero2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(oportunidades, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(36, 36, 36)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(respuesta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(darRespuesta))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Random r=new Random();
        int vida=3;
        int max= 9;
        int min= 0;
        int max2=9;
        int min2=0;
        int x=0;
        int num=r.nextInt(max-min)+min;
        int num2=r.nextInt(max2-min2)+min2;
        int op = (int) (Math.random() * 3);
        this.numero1.setText(Integer.toString(num));
        this.numero2.setText(Integer.toString(num2));
        this.oportunidades.setText("3");
        this.correcta.setText("0");
        oportunidad = 3;
        signo.setText(operadores[op] + "");
        darRespuesta.setEnabled(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void darRespuestaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_darRespuestaActionPerformed
        int num1 = Integer.parseInt(numero1.getText());
        int num2 = Integer.parseInt(numero2.getText());
        int resp = Integer.parseInt(respuesta.getText());
        String op = signo.getText();
        switch(op){
            case "+":
                if(num1 + num2 == resp){
                    JOptionPane.showMessageDialog(null, "Respuesta Correcta");
                    aciertos++;
                    correcta.setText(aciertos + "");
                    nuevo();
                }else{
                    JOptionPane.showMessageDialog(null, "Respuesta Incorrecta");
                    oportunidad--;
                    respuesta.setText("");
                    oportunidades.setText("" + oportunidad);
                }
                break;
            case "-":
                if(num1 - num2 == resp){
                    JOptionPane.showMessageDialog(null, "Respuesta Correcta");
                    aciertos++;
                    correcta.setText(aciertos + "");
                    
                    nuevo();
                }else{
                    JOptionPane.showMessageDialog(null, "Respuesta Incorrecta");
                    oportunidad--;
                    respuesta.setText("");
                    oportunidades.setText("" + oportunidad);
                }
                break;
                
            case "*":
                if(num1 * num2 == resp){
                    JOptionPane.showMessageDialog(null, "Respuesta Correcta");
                    aciertos++;
                    correcta.setText(aciertos + "");
                    
                    nuevo();
                }else{
                    JOptionPane.showMessageDialog(null, "Respuesta Incorrecta");
                    oportunidad--;
                    respuesta.setText("");
                    oportunidades.setText("" + oportunidad);
                }
                break;
            case "/":
                if(num1 / num2 == resp){
                    JOptionPane.showMessageDialog(null, "Respuesta Correcta");
                    aciertos++;
                    correcta.setText(aciertos + "");
                    nuevo();
                }else{
                    JOptionPane.showMessageDialog(null, "Respuesta Incorrecta");
                    oportunidad--;
                    respuesta.setText("");
                    oportunidades.setText("" + oportunidad);
                }
                break;
        }
        if(oportunidad == 0){
            JOptionPane.showMessageDialog(null, "Ya no te quedan oportunidades, PERDISTE!");
            darRespuesta.setEnabled(false);
        }
        
    }//GEN-LAST:event_darRespuestaActionPerformed

    public void nuevo(){
        Random r=new Random();
        int vida=3;
        int max= 9;
        int min= 0;
        int max2=9;
        int min2=0;
        int x=0;
        int num=r.nextInt(max-min)+min;
        int num2=r.nextInt(max2-min2)+min2;
        int op = (int) (Math.random() * 3);
        this.numero1.setText(Integer.toString(num));
        this.numero2.setText(Integer.toString(num2));
        signo.setText(operadores[op] + "");
        respuesta.setText("");
    }
    
    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField correcta;
    private javax.swing.JButton darRespuesta;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField numero1;
    private javax.swing.JTextField numero2;
    private javax.swing.JTextField oportunidades;
    private javax.swing.JTextField respuesta;
    private javax.swing.JTextField signo;
    // End of variables declaration//GEN-END:variables
}
